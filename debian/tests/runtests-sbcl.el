(setq slime-lisp-implementations '((sbcl ("sbcl" "--no-userinit" "--no-sysinit" "--eval" "(require :asdf)" "--eval" "(setq asdf:*user-cache* (uiop:getenv \"AUTOPKGTEST_TMP\"))"))))
(setq slime-repl-history-file (concat (getenv "AUTOPKGTEST_TMP") "/.slime-history.eld"))

(require 'slime-tests)
(slime-setup)
(slime-batch-test)
